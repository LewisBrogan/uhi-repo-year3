**Date**: Aug 31, 2023

**Guest Lecturer**: John McNamara

**Company**: IBM

John McNamara who is a master inventor at IBM provided heaps of insightful information during his guest talk. He explored not only the intricacies of tech, Artifical Intelligence, but also dove into indispensable social skills typically bypassed yet vital for professional progression.

Here's a breakdown of the talk:

1. **The Power of Credentials**: Mentioned how credentials such as IBM certficiations can stand out to employers in such a competitive market to give you the edge, also they can be obtained free of cost by students.

2. **Free Tools Are Your Friend**: Make the most of the free access we have to IBM's premium technologies and tools. They can be game-changers when we're job hunting.

3. **Problem-Solving Isn't Just for Techies**: One of the most relatable parts was when John talked about the universal importance of problem-solving. Whether you're a coder or a history major, being able to tackle real-world issues is a skill that employers crave. John even suggested that we could use these skills to help out local charities or university societies.

4. **Life Happens, Adapt**: He shared some personal stories on how he adapted to failure and quick thinking. He talked about a client presentation that initially went south but was saved by quick thinking and problem-solving. It showed us that we will experience setbacks in our career, but it's how we deal with these setbacks that will help us.

5. **Don't Be Intimidated**: Basically, don't be intimidated by all the tools and technologies at hand, just have a go and make something.

I enjoyed his talk. He gave us practical advice that we can start applying today, whether it's earning a new badge or tackling a real-world problem. Also the important of overcoming setbacks in your career and handling them.

---

References:

* Webex meeting recording: Induction-20230831 0911-1
Password: BfMqkW3f
Recording link: https://uhi.webex.com/recordingservice/sites/uhi/recording/playback/4485e03b2a0c103cbaef00505681ae7a